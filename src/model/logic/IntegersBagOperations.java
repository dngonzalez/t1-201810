package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	
	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE, value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	
	
	public int getMin(IntegersBag bag){
		int min = Integer.MAX_VALUE, value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if(min > value)
					min = value;
			}
		}
		return min;
	}
	
	/**
	 * 
	 * @return -1 if the bag is empty, if not it will give the adition of all the numbers.
	 */
	public int getSum(IntegersBag bag){
		int sum = -1;
		if(bag != null){
			sum = 0;
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
				sum += iter.next();
		}
		return sum;
	}
	
	/**
	 * 
	 * @return -1 if the bag is empty, if not it will give the adition of all the numbers.
	 */
	public int getMult(IntegersBag bag){
		int sum = -1;
		if(bag != null){
			sum = 1;
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
				sum = sum * iter.next();
		}
		return sum;
	}
}
